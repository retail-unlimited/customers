module gitlab.com/retail-unlimited/customers

go 1.17

require (
	github.com/matryer/moq v0.2.5
	github.com/stretchr/testify v1.7.0
	gitlab.com/retail-unlimited/apihub v0.0.0-20220311110113-9effe984d563
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/twitchtv/twirp v8.1.1+incompatible // indirect
	golang.org/x/mod v0.3.0 // indirect
	golang.org/x/tools v0.0.0-20200815165600-90abf76919f3 // indirect
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)

//replace gitlab.com/retail-unlimited/apihub => ../apihub
