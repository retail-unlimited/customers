package customers

import "time"

// CustomerProvisioned signifies that a new customer has been provisioned
type CustomerProvisioned struct {
	ID          string
	HappenedOn  time.Time
	CustomerID  string
	FirstName   string
	LastName    string
	CompanyName string
}
