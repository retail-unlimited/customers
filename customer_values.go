package customers

// NewCustomerID creates new customer id
func NewCustomerID() CustomerID {
	return "customer-456-abc"
}

// CustomerID represents customer identity
type CustomerID string

func (id CustomerID) String() string {
	return string(id)
}

// NewCustomerName creates customer name
func NewCustomerName(firstName string, lastName string, companyName string) (CustomerName, error) {
	// TODO checks

	return CustomerName{
		FirstName:   firstName,
		LastName:    lastName,
		CompanyName: companyName,
	}, nil
}

// CustomerName represents full customer name
type CustomerName struct {
	FirstName   string
	LastName    string
	CompanyName string
}
