package test

import (
	"gitlab.com/retail-unlimited/customers"
	"testing"
)

// NewCustomer builds a customer based on the builder options provided
func NewCustomer(t *testing.T, opts ...CustomerOpt) (*customers.Customer, CustomerBuilder) {
	cb := defaultCustomerBuilder(t)

	for _, opt := range opts {
		cb = opt(cb)
	}

	name, err := customers.NewCustomerName(cb.FirstName, cb.LastName, cb.CompanyName)
	if err != nil {
		t.Fatal(err)
	}

	customer, err := customers.NewCustomer(cb.ID, name)
	if err != nil {
		cb.t.Fatal(err)
	}

	return customer, CustomerBuilder{}
}

func defaultCustomerBuilder(t *testing.T) CustomerBuilder {
	return CustomerBuilder{
		t:           t,
		ID:          customers.NewCustomerID(),
		FirstName:   "John",
		LastName:    "Doe",
		CompanyName: "Doe Ltd.",
	}
}

// CustomerOpt used with customer test builder
type CustomerOpt func(builder CustomerBuilder) CustomerBuilder

// CustomerBuilder used for building test customers
type CustomerBuilder struct {
	t *testing.T

	ID          customers.CustomerID
	FirstName   string
	LastName    string
	CompanyName string
}

// WithPersonalName option
func WithPersonalName(firstName, lastName string) CustomerOpt {
	return func(b CustomerBuilder) CustomerBuilder {
		b.FirstName = firstName
		b.LastName = lastName

		return b
	}
}

// WithCompanyName option
func WithCompanyName(name string) CustomerOpt {
	return func(b CustomerBuilder) CustomerBuilder {
		b.CompanyName = name

		return b
	}
}

// NewCustomerFromArgentina represents an object mother
func NewCustomerFromArgentina(t *testing.T) *customers.Customer {
	return nil
}
