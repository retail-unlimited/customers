package customers

import "context"

// ProvisionCustomerFunc represents customer provisioning use case
type ProvisionCustomerFunc func(context.Context, *CustomerToProvision) (*ProvisionedCustomer, error)

// CustomerToProvision describes a customer to provision
type CustomerToProvision struct {
	FirstName   string
	LastName    string
	CompanyName string
}

// ProvisionedCustomer represents a provisioned customer response
type ProvisionedCustomer struct {
	ID string
}

// NewProvisionCustomerFunc creates new provision customer use case
func NewProvisionCustomerFunc(repo CustomerRepo) ProvisionCustomerFunc {
	return func(ctx context.Context, cmd *CustomerToProvision) (*ProvisionedCustomer, error) {
		id := NewCustomerID()

		name, err := NewCustomerName(cmd.FirstName, cmd.LastName, cmd.CompanyName)
		if err != nil {
			return nil, err
		}

		customer, err := NewCustomer(id, name)
		if err != nil {
			return nil, err
		}

		err = repo.Save(ctx, customer)
		if err != nil {
			return nil, err
		}

		return &ProvisionedCustomer{
			ID: customer.ID.String(),
		}, nil
	}
}
