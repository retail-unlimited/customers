package customers

import (
	"context"
)

// CustomerRepo represents customer repository used for retrieving and persisting customers
//go:generate moq -pkg mocks -out test/mocks/customer_repo.go . CustomerRepo
type CustomerRepo interface {
	Save(ctx context.Context, customer *Customer) error
}

// NewCustomer creates new customer
func NewCustomer(id CustomerID, name CustomerName) (*Customer, error) {
	return &Customer{
		ID:   id,
		Name: name,
	}, nil
}

// Customer represents a customer aggregate
type Customer struct {
	ID   CustomerID
	Name CustomerName
}
