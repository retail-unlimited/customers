package twirp_test

import (
	"context"
	"github.com/stretchr/testify/assert"
	pb "gitlab.com/retail-unlimited/apihub/rpc/customers"
	"gitlab.com/retail-unlimited/customers"
	"gitlab.com/retail-unlimited/customers/twirp"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestCustomer_Provisioning_UseCase_Is_Called(t *testing.T) {
	id := "id-123"
	firstName := "John"
	lastName := "Doe"

	var cmd *customers.CustomerToProvision

	c, clean := setupProvisionCustomerServer(
		func(ctx context.Context, cp *customers.CustomerToProvision) (*customers.ProvisionedCustomer, error) {
			cmd = cp

			return &customers.ProvisionedCustomer{
				ID: id,
			}, nil
		},
	)

	defer clean()

	pc, err := c.ProvisionCustomer(context.TODO(), &pb.CustomerToProvision{
		FirstName: firstName,
		LastName:  lastName,
	})
	assert.NoError(t, err)

	assert.NotEqual(t, pc.Id, id)
	assert.Equal(t, firstName, cmd.FirstName)
	assert.Equal(t, lastName, cmd.LastName)

}

func setupProvisionCustomerServer(pc customers.ProvisionCustomerFunc) (pb.Customers, func()) {
	url, clean := setup(
		twirp.NewCustomersService(pc),
	)

	return pb.NewCustomersProtobufClient(url, http.DefaultClient), clean
}

func setup(svc pb.Customers) (string, func()) {
	handler := pb.NewCustomersServer(svc, nil)

	s := httptest.NewServer(handler)

	return s.URL, func() {
		s.Close()
	}
}
