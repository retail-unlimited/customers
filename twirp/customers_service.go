package twirp

import (
	"context"
	pb "gitlab.com/retail-unlimited/apihub/rpc/customers"
	"gitlab.com/retail-unlimited/customers"
)

// NewCustomersService creates new customers service
func NewCustomersService(provisionCustomer customers.ProvisionCustomerFunc) *CustomersService {
	return &CustomersService{provisionCustomer: provisionCustomer}
}

// CustomersService represents customers api entry point (twirp implementation)
type CustomersService struct {
	provisionCustomer customers.ProvisionCustomerFunc
}

// ProvisionCustomer invokes provision customer use case
func (s CustomersService) ProvisionCustomer(
	ctx context.Context, req *pb.CustomerToProvision) (*pb.ProvisionedCustomer, error) {

	// TODO Only handle mapping and status codes
	// TODO Intercept domain errors

	pc, err := s.provisionCustomer(
		ctx,
		&customers.CustomerToProvision{
			FirstName: req.FirstName,
			LastName:  req.LastName,
		},
	)
	if err != nil {
		return nil, err
	}

	return &pb.ProvisionedCustomer{
		Id: pc.ID,
	}, nil
}
