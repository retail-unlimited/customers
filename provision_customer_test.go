package customers_test

import (
	"context"
	"github.com/stretchr/testify/assert"
	"gitlab.com/retail-unlimited/customers"
	"gitlab.com/retail-unlimited/customers/test/mocks"
	"testing"
)

func TestShould_Provision_A_Customer(t *testing.T) {
	var savedCustomer *customers.Customer

	repo := mocks.CustomerRepoMock{
		SaveFunc: func(_ context.Context, customer *customers.Customer) error {
			savedCustomer = customer

			return nil
		},
	}

	toProvision := customers.CustomerToProvision{
		FirstName: "John",
		LastName:  "Doe",
	}

	provision := customers.NewProvisionCustomerFunc(&repo)

	provisioned, err := provision(context.TODO(), &toProvision)
	assert.NoError(t, err)

	assert.Equal(t, provisioned.ID, savedCustomer.ID.String())
	assert.Equal(t, toProvision.FirstName, savedCustomer.Name.FirstName)
	assert.Equal(t, toProvision.LastName, savedCustomer.Name.LastName)
}

// TODO TDD customer details

// TODO Test error type and table test error classes -> unit test the domain model for specific cases
