package ram

import (
	"context"
	"gitlab.com/retail-unlimited/customers"
)

// NewCustomerRepo new customer in memory repo
func NewCustomerRepo() *CustomerRepo {
	return &CustomerRepo{
		customers: make(map[customers.CustomerID]customers.Customer),
	}
}

// CustomerRepo represents customers in memory repo
type CustomerRepo struct {
	customers map[customers.CustomerID]customers.Customer
}

// Save saves a customer
func (c CustomerRepo) Save(ctx context.Context, customer *customers.Customer) error {
	c.customers[customer.ID] = *customer

	return nil
}
