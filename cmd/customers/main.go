package main

import (
	pb "gitlab.com/retail-unlimited/apihub/rpc/customers"
	"gitlab.com/retail-unlimited/customers"
	"gitlab.com/retail-unlimited/customers/ram"
	"gitlab.com/retail-unlimited/customers/twirp"
	"log"
	"net/http"
)

func main() {
	repo := ram.NewCustomerRepo()

	handler := pb.NewCustomersServer(
		twirp.NewCustomersService(
			customers.NewProvisionCustomerFunc(repo),
		),
		// hooks from kit package
		nil,
	)

	log.Fatal(http.ListenAndServe(":8888", handler))
}
